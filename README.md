
# Installation

## Install backend
```
cd backend
npm install
```

## Create tables
```
cd backend
npm run create-tables
```

## Install frontend
```
cd frontend
npm install
```


# Execution

## Start server
```
cd backend
npm run dev
```

## Start client
```
cd frontend
npm run serve
```

Open browser on localhost:8080
Open multiple tabs to see real-time effects

