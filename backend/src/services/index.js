
// database
const itemsService = require('./database/items/items.service.js')

// custom
// ...

module.exports = function (app) {

   // database
   app.configure(itemsService)

   // custom
   // ...
}
