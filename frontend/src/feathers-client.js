
import io from 'socket.io-client'
import feathers from '@feathersjs/client'

const app = feathers()

// create persistent websocket with server
const socket = io()
app.configure(feathers.socketio(socket))

export default app
